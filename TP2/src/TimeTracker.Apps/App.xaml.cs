﻿using Storm.Mvvm;
using TimeTracker.Apps.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace TimeTracker.Apps
{
    public partial class App : MvvmApplication
    {
        public App() : base(CreateStartPage)
        {
            InitializeComponent();
            MainPage = new NavigationPage(new Connexion());

        }

        private static Page CreateStartPage()
        {
            return new MainPage();
        }

      
    }
}