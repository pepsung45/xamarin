﻿using System;
using System.Collections.Generic;

namespace TimeTracker.Apps.Models
{
    public class Task
    {
        public readonly int id;
        public int projectId;
        public string name;
        public List<Time> times;

        public Task(int id, string name, List<Time> times)
        {
            this.id = id;
            this.name = name;
            this.times = times;
        }

        public override string ToString()
        {
            return name;
        }

        public string TaskName { get => name; }
        public int TaskId { get => id; }

        public List<Time> Times { get => times; }

        // Donne la durée totale de la Task, utilisé dans GraphesViewModel.cs
        public float GetDuration()
        {
            int duration = 0;
            for (int j = 0; j < times.Count; j++)
            {
                duration += DateTime.Parse(times[j].EndTime).Subtract(DateTime.Parse(times[j].StartTime)).Seconds;
            }
            return duration;
        }

    }
}