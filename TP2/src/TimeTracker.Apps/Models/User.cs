﻿using System.Collections.Generic;
using System.Windows.Input;
namespace TimeTracker.Apps.Models
{
    public class User
    {
        public string email;
        public string last_name;
        public string first_name;

        public User(string email, string last_name, string first_name)
        {
            this.email = email;
            this.last_name = last_name;
            this.first_name = first_name;
        }
        public string Email { get => email; set => email = value; }
        public string LastName { get => last_name; set => last_name = value; }
        public string FirstName { get => first_name; set => first_name = value; }

        public override string ToString()
        {
            return email + "\n" + last_name + "\n" + first_name;
        }

    }
}