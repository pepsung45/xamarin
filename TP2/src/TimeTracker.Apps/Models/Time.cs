﻿
using System;

namespace TimeTracker.Apps.Models
{
    public class Time
    {
        public readonly int id;
        public DateTime start_time;
        public DateTime end_time;

        public Time(int id, DateTime start_time, DateTime end_time)
        {
            this.id = id;
            this.start_time = start_time;
            this.end_time = end_time;
           
        }

        public string StartTime
        {
            get => start_time.ToString();
        }

        public string EndTime
        {
            get => end_time.ToString();
        }

    }
}
