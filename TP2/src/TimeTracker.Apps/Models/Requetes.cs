﻿using Nancy.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Dtos;
using Xamarin.Essentials;

namespace TimeTracker.Apps.Models
{
    // Classe static hébergeant toutes les requêtes à l'API 
    public static class Requetes
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string urlBase = Urls.HOST + '/';

        public static async Task<List<Project>> GetProjects()
        {
            string url = urlBase + Urls.LIST_PROJECTS;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject json = JObject.Parse(result);
            List<Project> projets = new List<Project>();
            var list = json.GetValue("data");
            foreach(var value in list)
            {
                projets.Add(value.ToObject<Project>());
            }

            return projets;
        }


        public static async Task<List<Task>> GetTasks(int id)
        {
            string url = urlBase + Urls.LIST_TASKS;
           

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", id.ToString()));
            request.Method = HttpMethod.Get;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject json = JObject.Parse(result);
            List<Task> tasks = new List<Task>();
            var list = json.GetValue("data");
            foreach (var value in list)
            {
                Task t = value.ToObject<Task>();
                t.projectId = id;
                tasks.Add(t);
            }

            return tasks;
        }


        public static async Task<string> Connexion(string login, string mdp)
        {
            string url = urlBase + Urls.LOGIN;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Post;

            string json = new JavaScriptSerializer().Serialize(new
            {
                login = login,
                password = mdp,
                client_id = "MOBILE",
                client_secret = "COURS"
            });

            StringContent stringContent = new StringContent(json,Encoding.UTF8,"application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            return result;
        }


        public static async Task<string> Inscription(string firstName, string name, string email, string mdp)
        {
            string url = urlBase + Urls.CREATE_USER;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Post;

            string json = new JavaScriptSerializer().Serialize(new
            {
                first_name = firstName,
                last_name = name,
                email = email,
                password = mdp,
                client_id = "MOBILE",
                client_secret = "COURS"
            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            return result;
        }


        public static async Task<Project> CreateProject(string name, string description)
        {
            string url = urlBase + Urls.ADD_PROJECT;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Post;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            string json = new JavaScriptSerializer().Serialize(new
            {
                name = name,
                description = description
            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject jsonObject = JObject.Parse(result);

            return jsonObject.GetValue("data").ToObject<Project>();
        }


        public static async Task<Project> EditProject(string name, string description, int id)
        {
            string url = urlBase + Urls.UPDATE_PROJECT;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}",id.ToString()));
            request.Method = HttpMethod.Put;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            string json = new JavaScriptSerializer().Serialize(new
            {
                name = name,
                description = description
            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject jsonObject = JObject.Parse(result);

            return jsonObject.GetValue("data").ToObject<Project>();
        }


        public static async void DeleteProject(int id)
        {
            string url = urlBase + Urls.DELETE_PROJECT;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", id.ToString()));
            request.Method = HttpMethod.Delete;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }


        public static async Task<Task> PostTask(string name, int id)
        {
            string url = urlBase + Urls.CREATE_TASK;
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", id.ToString()));
            request.Method = HttpMethod.Post;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            string json = new JavaScriptSerializer().Serialize(new
            {
                name = name,
               
            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject jsonObject = JObject.Parse(result);

            Task t = jsonObject.GetValue("data").ToObject<Task>();
            t.projectId = id;

            return t;
        }


        public static async Task<Task> EditTask(string name, int taskId, int projectId)
        {
            string url = urlBase + Urls.UPDATE_TASK;
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", projectId.ToString()).Replace("{taskId}",taskId.ToString()));
            request.Method = HttpMethod.Put;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            string json = new JavaScriptSerializer().Serialize(new
            {
                name = name,
            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject jsonObject = JObject.Parse(result);

            Task t = jsonObject.GetValue("data").ToObject<Task>();
            t.projectId = projectId;

            return t;
        }


        public static async void DeleteTask(int taskId, int projectId)
        {
            string url = urlBase + Urls.DELETE_TASK;
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", projectId.ToString()).Replace("{taskId}", taskId.ToString()));
            request.Method = HttpMethod.Delete;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        public static async Task<User> GetUser()
        {
            string url = urlBase + Urls.USER_PROFILE;

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            User user;
            JObject jsonObject = JObject.Parse(result);
            user = jsonObject.GetValue("data").ToObject<User>();

            return user;
        }

        public static async Task<Task> PostTimer(int taskId, int projectId, string start, string end)
        {
            string url = urlBase + Urls.ADD_TIME;
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url.Replace("{projectId}", projectId.ToString()).Replace("{taskId}", taskId.ToString()));
            request.Method = HttpMethod.Post;
            request.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));

            string json = new JavaScriptSerializer().Serialize(new
            {
                start_time = start,
                end_time = end

            });

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            request.Content = stringContent;

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            JObject jsonObject = JObject.Parse(result);

            Task t = jsonObject.GetValue("data").ToObject<Task>();
            t.projectId = projectId;

            return t;
        }


        public static async Task<User> PatchUser(string email, string first_name, string last_name)
        {
            string url = urlBase + Urls.USER_PROFILE;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));
            httpWebRequest.Method = "PATCH";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.UseDefaultCredentials = true;
            httpWebRequest.ServerCertificateValidationCallback = delegate { return true; };

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    email = email,
                    first_name = first_name,
                    last_name = last_name,

                });
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            JObject jsonObject = JObject.Parse(result);

            User user = jsonObject.GetValue("data").ToObject<User>();

            return user;
        }


        public static async void PatchPassword(string old, string newPassword)
        {
            string url = urlBase + Urls.SET_PASSWORD;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers.Add("Authorization", "Bearer " + Preferences.Get("access_token", "o"));
            httpWebRequest.Method = "PATCH";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.UseDefaultCredentials = true;
            httpWebRequest.ServerCertificateValidationCallback = delegate { return true; };
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    old_password = old,
                    new_password = newPassword

                });
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        }
    }
}
