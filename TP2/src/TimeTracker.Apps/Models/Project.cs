﻿
namespace TimeTracker.Apps.Models
{
    public class Project
    {
        public readonly int id;
        public string name;
        public string description;
        public int total_seconds;

        public Project(int id, string name, string description, int total_seconds)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.total_seconds = total_seconds;
        }

        public string Name { get => name; }

        public string Description { get => description; }

        public int Total_Seconds { get => total_seconds; }

        public override string ToString()
        {
            return name + "\n" + description;
        }

    }
}