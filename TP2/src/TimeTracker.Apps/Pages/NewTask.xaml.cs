﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Apps.Models;
using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewTask : ContentPage
	{
		public NewTask (Project project)
		{

			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			BindingContext = new NewTaskViewModel(project);
		}
	}
}