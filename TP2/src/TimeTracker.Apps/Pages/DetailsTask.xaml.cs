﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeTracker.Apps.ViewModels;
using Task = TimeTracker.Apps.Models.Task;

namespace TimeTracker.Apps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailsTask : ContentPage
    {
        public DetailsTask(Task task)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new DetailsTaskViewModel(task);
        }
    }
}