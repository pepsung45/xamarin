using System;
using System.Collections.Generic;
using Storm.Mvvm;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;
using Task = TimeTracker.Apps.Models.Task;
using System.Diagnostics;
using System.ComponentModel;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class DetailsProjectViewModel : ViewModelBase
    {
        private Project project;
        private List<Task> tasks;
        private bool edition = false;
        Stopwatch stopWatch = new Stopwatch();
        private string _response;
        private string _timerText;
        private bool isAttached = false;
        private bool started = false;
        private string start_time;
        private string stop_timed;
        public DetailsProjectViewModel(Project project)
        {
            this.project = project;
            Modifier = new Command(EditProject);
            Supprimer = new Command(DeleteProject);
            AddTask = new Command(AjouterTask);
            StartStop = new Command(StartStopButton);
            StartTimer = new Command(StartTime);
            StopTimer = new Command(StopTime);
            _timerText = "START";
            ListTasks();
        }

        private string _stopWatchHours = "0";
        private string _stopWatchMinutes = "0";
        private string _stopWatchSeconds = "0";
        public string TextButton
        {
            get => started ? "STOP" : "START";
        }

        public string StopWatchHours
        {

            get => _stopWatchHours;
            set => SetProperty(ref _stopWatchHours, value);

        }
        public string StopWatchMinutes
        {

            get => _stopWatchMinutes;
            set => SetProperty(ref _stopWatchMinutes, value);
        }
        public string StopWatchSeconds
        {
            get => _stopWatchHours + ":" + _stopWatchMinutes + ":" + _stopWatchSeconds;
            set => SetProperty(ref _stopWatchSeconds, value);

        }

        public string Name
        {
            get => project.name;
            set => SetProperty(ref project.name, value);
        }

        public string Description
        {
            get => project.description;
            set => SetProperty(ref project.description, value);
        }


        public List<Task> Tasks
        {
            get => tasks;
            set => SetProperty(ref tasks, value);
        }

        public bool Edition
        {
            get => !edition;
            set {
                SetProperty(ref edition, value);
                OnPropertyChanged("TextModifier");
            }
        }

        public string TextModifier
        {
            get => edition ? "Sauvegarder" : "Modifier";
        }

        public string Response
        {
            get => _response;
            set => SetProperty(ref _response, value);
        }
        public string TimerText
        {
            get => _timerText;
            set => SetProperty(ref _timerText, value);
        }
        public bool IsAttached
        {
            get => isAttached;
            set => SetProperty(ref isAttached, value);
        }
        public Task SelectedTask
        {
            set
            {
                if (value == null) return;
                if (isAttached)
                    AttacherTimer(value);
                else
                    DetailsTache(value);
            }
        }

        private async void DetailsTache(Task task)
        {
            await App.Current.MainPage.Navigation.PushAsync(new DetailsTask(task));
        }

        private async void AttacherTimer(Task t)
        {
            await Requetes.PostTimer(t.id, t.projectId, start_time, stop_timed);
            IsAttached = false;
            await App.Current.MainPage.Navigation.PushAsync(new DetailsTask(t));
        }

        private void StartStopButton()
        {
            if (started) StopTime();
            else StartTime();
        }

        private void StartTime()
        {
            started = true;
            TimerText = "STOP";
            OnPropertyChanged("TextButton");
            stopWatch.Start();
            start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            StopWatchHours = stopWatch.Elapsed.Hours.ToString();
            StopWatchMinutes = stopWatch.Elapsed.Minutes.ToString();
            StopWatchSeconds = stopWatch.Elapsed.Seconds.ToString();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                StopWatchHours = stopWatch.Elapsed.Hours.ToString();
                StopWatchMinutes = stopWatch.Elapsed.Minutes.ToString();
                StopWatchSeconds = stopWatch.Elapsed.Seconds.ToString();
                return true;
            });
        }


        private void StopTime()
        {
            started = false;
            TimerText = "START";
            OnPropertyChanged("TextButton");
            stop_timed = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            Response = stopWatch.Elapsed.Hours.ToString();
            stopWatch.Stop();
            stopWatch.Reset();
            StopWatchSeconds = "0";
            StopWatchMinutes = "0";
            StopWatchHours = "0";
            IsAttached = true;
            //await Requetes.PostTask();
        }
        public ICommand Modifier { get; }
        public ICommand Supprimer { get; }
        public ICommand AddTask { get; }
        public ICommand StartTimer { get; }
        public ICommand StopTimer { get; }
        public ICommand StartStop { get; }
        private async void EditProject()
        {
            if (!edition)
            {
                Edition = true;
            }
            else
            {
                project = await Requetes.EditProject(Name, Description, project.id);
                await App.Current.MainPage.Navigation.PushAsync(new Accueil());
            }
            
        }

        private async void DeleteProject()
        {
            Requetes.DeleteProject(project.id);
            await App.Current.MainPage.Navigation.PushAsync(new Accueil());
        }

        private async void AjouterTask()
        {
            
            await App.Current.MainPage.Navigation.PushAsync(new NewTask(project));
        }

        private async void ListTasks()
        {
            Tasks = await Requetes.GetTasks(project.id);
        }


    }
}