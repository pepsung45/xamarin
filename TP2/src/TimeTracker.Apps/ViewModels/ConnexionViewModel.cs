﻿using System;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using System.Net.Http;
using TimeTracker.Apps.Pages;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;
using TimeTracker.Apps.Models;

namespace TimeTracker.Apps.ViewModels
{
    class ConnexionViewModel : ViewModelBase
    {
        private string _login;
        private string _pass;

        public ConnexionViewModel()
        {
            Connect = new Command(Connection);
        }

        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }

        public string Password
        {
            get => _pass;
            set => SetProperty(ref _pass, value);
        }

        // Pour ne pas que les fields restent remplis après avoir quitté la page (sécurité)
        private void ClearFields()
        {
            Login = "";
            Password = "";
        }

        public ICommand Connect { get; }

        public ICommand GoToInscription => new Command(async () =>
        {
            ClearFields();
            await App.Current.MainPage.Navigation.PushAsync(new Inscription());
        });

        private async void Connection()
        {

            try
            {
                string reponse = await Requetes.Connexion(Login, Password);

                JObject json = JObject.Parse(reponse);
                var access_token = json.GetValue("data")["access_token"];
                Preferences.Set("access_token", access_token.ToString());
                ClearFields();
                await App.Current.MainPage.Navigation.PushAsync(new Accueil());
            }
            catch(Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Oh oh ! Une erreur est survenue !", "Cette erreur peut être dûe à un problème de connexion à Internet, ou à de mauvais identifiants de connexion.", "Retour");
            }
          
        }


    }
}
