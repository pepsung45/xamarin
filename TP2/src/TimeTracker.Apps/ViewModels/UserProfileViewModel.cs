﻿using Storm.Mvvm;
using System;
using System.Windows.Input;
using TimeTracker.Apps.Models;
using TimeTracker.Apps.Pages;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    internal class UserProfileViewModel : ViewModelBase
    {
        private User _user;

        public UserProfileViewModel()
        {
            _user = new User(" ", "", "");
            UpdateInfo = new Command(PatchInfo);
            goToChangerMdp = new Command(GoToChangerMdp);
            GetResponseRequest();
        }

        public ICommand UpdateInfo { get; }
        public ICommand goToChangerMdp { get; }
        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        private async void PatchInfo()
        {

            User = await Requetes.PatchUser(User.Email, User.FirstName, User.LastName);
            await App.Current.MainPage.Navigation.PushAsync(new Accueil());

        }

        private async void GetResponseRequest()
        {
            User = await Requetes.GetUser();
        }

        private async void GoToChangerMdp()
        {
            await App.Current.MainPage.Navigation.PushAsync(new ChangerMDP());
        }

    }
}
