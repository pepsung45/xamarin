using System;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;

namespace TimeTracker.Apps.ViewModels
{
    public class NewProjectViewModel : ViewModelBase
    {
        private string name;
        private string description;
        private string response;
       
        public NewProjectViewModel()
        {
            Create = new Command(CreerProjet);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public string Response
        {
            get => response;
            set => SetProperty(ref response, value);
        }

        public ICommand Create { get; }

        private async void CreerProjet()
        {
            try
            {
                if(name.Length < 1 || description.Length < 1)
                {
                    Response = "Les champs doivent �tre remplis !";
                    return;
                }
                Project p = await Requetes.CreateProject(Name, Description);
                await App.Current.MainPage.Navigation.PushAsync(new DetailsProject(p));
            }catch(Exception e)
            {
                Response = "Erreur cr�ation projet : "+e.Message;
            }
        }


        
    }
}