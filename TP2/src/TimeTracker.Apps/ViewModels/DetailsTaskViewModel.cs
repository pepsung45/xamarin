using System;
using System.Collections.Generic;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;
using Task = TimeTracker.Apps.Models.Task;
using System.Timers;
using System.Diagnostics;

namespace TimeTracker.Apps.ViewModels
{
    public class DetailsTaskViewModel : ViewModelBase
    {
        private Task task;
        private bool edition = false;
        private bool started = false;
        Stopwatch stopWatch = new Stopwatch();
        private string _response;
        private string start_time;
        public DetailsTaskViewModel(Task task)
        {
            this.task = task;
            Modifier = new Command(EditTask);
            Supprimer = new Command(DeleteTask);
            StartStop = new Command(StartStopTime);
            RefreshTask();
        }


        private string _stopWatchHours = "0";
        private string _stopWatchMinutes = "0";
        private string _stopWatchSeconds = "0";

        public string StopWatchHours
        {

            get => _stopWatchHours;
            set => SetProperty(ref _stopWatchHours, value);

        }
        public string StopWatchMinutes
        {

            get => _stopWatchMinutes;
            set => SetProperty(ref _stopWatchMinutes, value);



        }
        public string StopWatchSeconds
        {
            get => _stopWatchHours + ":" + _stopWatchMinutes + ":" + _stopWatchSeconds;
            set => SetProperty(ref _stopWatchSeconds, value);
        }


        public string Name
        {
            get => task.name;
            set => SetProperty(ref task.name, value);
        }

        public string Response
        {
            get => _response;
            set => SetProperty(ref _response, value);
        }

        public List<Time> Times
        {
            get => Task.times;
        }

        public bool Edition
        {
            get => !edition;
            set {
                SetProperty(ref edition, value);
                OnPropertyChanged("TextModifier");
            }
        }

        public bool Started
        {
            get => started;
            set
            {
                SetProperty(ref started, value);
                OnPropertyChanged("TextButton");
            }
        }

        public string TextModifier
        {
            get => edition ? "Sauvegarder" : "Modifier";
        }

        public string TextButton
        {
            get => started ? "STOP" : "START";
        }

        public Task Task
        {
            get => task;
            set
            {
                SetProperty(ref task, value);
                OnPropertyChanged("Times");
            }
        }

        public ICommand Modifier { get; }
        public ICommand Supprimer { get; }
        public ICommand StartStop { get; }

        private void StartStopTime()
        {
            if (started) StopTime();
            else StartTime();
        }

        private void StartTime()
        {
            Started = true;
            stopWatch.Start();
            start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            StopWatchHours = stopWatch.Elapsed.Hours.ToString();
            StopWatchMinutes = stopWatch.Elapsed.Hours.ToString();
            StopWatchSeconds = stopWatch.Elapsed.Hours.ToString();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                StopWatchHours = stopWatch.Elapsed.Hours.ToString();
                StopWatchMinutes = stopWatch.Elapsed.Minutes.ToString();
                StopWatchSeconds = stopWatch.Elapsed.Seconds.ToString();
                return true;
            });
        }


        private async void StopTime()
        {
            Started = false;
            string end_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            Response = stopWatch.Elapsed.Seconds.ToString();
            stopWatch.Stop();
            stopWatch.Reset();
            StopWatchSeconds = "0";
            StopWatchMinutes = "0";
            StopWatchHours = "0";
            await Requetes.PostTimer(task.id, task.projectId, start_time, end_time );
            RefreshTask();
        }
        private async void EditTask()
        {
            if (!edition)
            {
                Edition = true;
            }
            else
            {
                task = await Requetes.EditTask(Name, task.id, task.projectId);
                await App.Current.MainPage.Navigation.PushAsync(new Accueil());
            }
            
        }

        private async void DeleteTask()
        {
            Requetes.DeleteTask(task.id, task.projectId);
            await App.Current.MainPage.Navigation.PushAsync(new Accueil());
        }

        private async void RefreshTask()
        {
            List<Task> tasks = await Requetes.GetTasks(task.projectId);
            foreach(Task t in tasks)
            {
                if (t.id == task.id)
                {
                    Task = t;
                }
            }
        }

        


        
    }
}