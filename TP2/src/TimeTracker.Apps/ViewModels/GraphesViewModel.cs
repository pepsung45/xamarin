﻿using System.Collections.Generic;
using Storm.Mvvm;
using Microcharts;
using TimeTracker.Apps.Models;
using Task = TimeTracker.Apps.Models.Task;
using SkiaSharp;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace TimeTracker.Apps.ViewModels
{
    public class GraphesViewModel : ViewModelBase
    {
        private List<Project> projects;
        private Dictionary<Project, List<Task>> taches;

        private int cpt = 0;
        private Chart _chartProjects;
        private ObservableCollection<Chart> _chartTasks;

        public GraphesViewModel()
        {
            projects = new List<Project>();
            taches = new Dictionary<Project, List<Task>>();
            _chartProjects = new PieChart();
            _chartTasks = new ObservableCollection<Chart>();
            ListProjects();
        }

        public Chart ChartProjects { get => _chartProjects; set => SetProperty(ref _chartProjects, value); }

        public ObservableCollection<Chart> ChartTasks { get => _chartTasks; set => SetProperty(ref _chartTasks, value); }

        private async void ListProjects()
        {
            projects = await Requetes.GetProjects();
            foreach(Project p in projects)
            {
                List<Task> t = await Requetes.GetTasks(p.id);
                taches.Add(p, t);
            }

            DrawGrapheProject(); 
            DrawGraphesTasks();
        }

        // On récupère les couleurs les unes après les autres à l'aide d'une variable globale, pour éviter 2 couleurs semblables
        private SKColor getColor()
        {
               List<string> colors = new List<string>
            {
                "FF5733", "FFBB33", "DEFF27", "48FF27", "27FF9D",
                "27FFF2", "278CFF", "4127FF", "8927FF", "C43DCF",
                "CF3D8D"
            };
            int nbcolors = colors.Count;
            return SKColor.Parse(colors[(cpt++) % nbcolors]);
        }

        private void DrawGrapheProject()
        {
            /* creation de la collection d'entry*/
            List<ChartEntry> chartEntries = new List<ChartEntry>();
         

            /* parcours des projets*/
            foreach (Project project in projects)
            {
                // On crée les chartEntries les unes après les autres
                chartEntries.Add(new ChartEntry(project.total_seconds)
                {
                    Label = project.name,
                    ValueLabel = project.total_seconds.ToString(),
                    Color = getColor()
                });
            }

            /* creation du piechart */
            ChartProjects = new PieChart { Entries = chartEntries, LabelTextSize = 35, LabelColor = SKColor.Parse("000000") };
        }

        private void DrawGraphesTasks()
        {

            for (int i = 0; i < projects.Count; i++)
            {
                List<ChartEntry> chartEntries = new List<ChartEntry>();
                taches.TryGetValue(projects[i], out List<Task> t);
                foreach (Task task in t)
                {
                    chartEntries.Add(new ChartEntry(task.GetDuration())
                    {
                        Label = task.name,
                        ValueLabel = task.id.ToString(),
                        Color = getColor()
                    });
                }
                ChartTasks.Add(new PieChart { Entries = chartEntries, LabelTextSize = 35, LabelColor = SKColor.Parse("000000") });
            }
        }
    }
}