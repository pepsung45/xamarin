﻿using System;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;

namespace TimeTracker.Apps.ViewModels
{
    class InscriptionViewModel : ViewModelBase
    {
        private string _login;
        private string _pass;
        private string _prenom;
        private string _nom;
        private string _response;

        public InscriptionViewModel()
        {
            Connect = new Command(Connection);
        }

        public string Prenom
        {
            get => _prenom;
            set => SetProperty(ref _prenom, value);
        }

        public string Nom
        {
            get => _nom;
            set => SetProperty(ref _nom, value);
        }

        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }

        public string Password
        {
            get => _pass;
            set => SetProperty(ref _pass, value);
        }

        public string Response
        {
            get => _response;
            set => SetProperty(ref _response, value);
        }

        public ICommand Connect { get; }

        public ICommand GoToConnexion => new Command(async () =>
        {
            ClearFields();
            await App.Current.MainPage.Navigation.PushAsync(new Connexion());
        });

        private void ClearFields()
        {
            Login = ""; Password = ""; Prenom = ""; Nom = "";
        }

        private async void Connection()
        {

            try
            {
                Response = await Requetes.Inscription(Prenom, Nom, Login, Password);

                JObject json = JObject.Parse(Response);
                var access_token = json.GetValue("data")["access_token"];
                Preferences.Set("access_token", access_token.ToString());
                ClearFields();
                await App.Current.MainPage.Navigation.PushAsync(new Accueil());
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Oh oh ! Une erreur est survenue !", "Cette erreur peut être dûe à un problème de connexion à Internet, ou à une adresse mail déjà utilisée.", "Retour");
            }


        }


    }
}
