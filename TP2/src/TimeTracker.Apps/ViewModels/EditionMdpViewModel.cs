﻿using System;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using System.Net.Http;
using TimeTracker.Dtos;
using System.Net;
using System.IO;
using Nancy.Json;
using Xamarin.Essentials;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;

namespace TimeTracker.Apps.ViewModels
{
     class EditionMdpViewModel : ViewModelBase
    {
        private string _newPassword;
        private string _oldPassword;
        private string _response;
        public EditionMdpViewModel()
        {
            ChangeMDP = new Command(ChangerMDP);
        }

        public string OldPassword
        {
            get => _oldPassword;
            set => SetProperty(ref _oldPassword, value);
        }
        
        public string NewPassword
        {
            get => _newPassword;
            set => SetProperty(ref _newPassword, value);
        }
        public string Response
        {
            get => _response;
            set => SetProperty(ref _response, value);
        }

        public ICommand ChangeMDP { get; }
        private async void ChangerMDP()
        {

            Requetes.PatchPassword(OldPassword, NewPassword);

            await App.Current.MainPage.Navigation.PushAsync(new Accueil());

        }
    }
}