﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Models;
using TimeTracker.Apps.Pages;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class NewTaskViewModel : ViewModelBase
    {
        private string name;
        private readonly Project project;
        private string response;

        public NewTaskViewModel(Project project)
        {
            this.project = project;
            Create = new Command(AjouterTache);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

 
        public string Response
        {
            get => response;
            set => SetProperty(ref response, value);
        }

        public ICommand Create { get; }

        private async void AjouterTache()
        {
            try
            {
                if (name.Length < 1)
                {
                    Response = "Les champs doivent être remplis !";
                    return;
                }
                await Requetes.PostTask(Name, project.id);
                await App.Current.MainPage.Navigation.PushAsync(new DetailsProject(project));
            }
            catch (Exception e)
            {
                Response = "Erreur création projet : " + e.Message;
            }
        }
    }
}