using System;
using System.Collections.Generic;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using TimeTracker.Apps.Pages;
using TimeTracker.Apps.Models;
using Task = TimeTracker.Apps.Models.Task;
using System.Diagnostics;
using System.ComponentModel;
using System.Timers;

namespace TimeTracker.Apps.ViewModels
{
    public class AccueilViewModel : ViewModelBase
    {
        private List<Project> _projets;
        private List<Task> _tasks;
        private string _response;
        private string _timerText;
        Stopwatch stopWatch = new Stopwatch();
        private bool isAttached = false;
        private bool started = false;
        private string start_time;
        private string stop_timed;
        public AccueilViewModel()
        {
            _projets = new List<Project>();
            _tasks = new List<Task>();
            NewProject = new Command(AddProject);
            Deconnexion = new Command(Logout);
            goToUserProfile = new Command(EditProfil);
            goToGraphes = new Command(GoToGraphes);
            StartStop = new Command(StartStopButton);
            _timerText = "START";
            
            GetResponseRequest();
        }

        private string _stopWatchHours = "0";
        private string _stopWatchMinutes = "0";
        private string _stopWatchSeconds = "0";
        public string TextButton
        {
            get => started ? "STOP" : "START";
        }

        public string StopWatchHours
        {
      
                get => _stopWatchHours;
                set => SetProperty(ref _stopWatchHours, value);
    
        }
        public string StopWatchMinutes
        {

                get => _stopWatchMinutes;
                set => SetProperty(ref _stopWatchMinutes, value);
        }
        public string StopWatchSeconds
        {
            get => _stopWatchHours + ":" + _stopWatchMinutes +":" + _stopWatchSeconds; 
            set => SetProperty(ref _stopWatchSeconds, value);

        }


        public bool IsAttached
        {
            get => isAttached;
            set => SetProperty(ref isAttached, value);
        }
        public List<Project> Projects {
            get => _projets;
            set => SetProperty(ref _projets, value);
        }

        public List<Task> Tasks
        {
            get => _tasks;
            set => SetProperty(ref _tasks, value);
        }
        public string Response
        {
            get => _response;
            set => SetProperty(ref _response, value);
        }
        public string TimerText
        {
            get => _timerText;
            set => SetProperty(ref _timerText, value);
        }

        public Project SelectedProject
        {
            set
            {
                if (value == null) return;
                
                ListTasks(value); // Lister les t�ches du projet s�lectionn�

                AskUser(value); // Demander � l'utilisateur s'il veut rejoindre la page du projet

            }
        }

        public Task SelectedTask
        {
            set
            {
                if (value == null) return;
                if (isAttached)
                    AttacherTimer(value); // Si on vient de finir un timer, on l'attache � la t�che
                else
                    DetailsTache(value); // Sinon, on renvoie vers la page de la t�che
            }
        }

        public ICommand NewProject { get; }
        public ICommand Deconnexion { get; }
        public ICommand Attacher { get; }
        public ICommand goToUserProfile { get; }
        public ICommand goToGraphes{ get; }
        public ICommand StartStop { get; }

        private async void AttacherTimer(Task t)
        {
            await Requetes.PostTimer(t.id, t.projectId, start_time, stop_timed);
            IsAttached = false;

            await App.Current.MainPage.Navigation.PushAsync(new DetailsTask(t));
        }

        private void StartStopButton()
        {
            if (started) StopTime();
            else StartTime();
        }

        private void StartTime()
        {
            started = true;
            TimerText = "STOP";
            OnPropertyChanged("TextButton");
            stopWatch.Start();
            start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            StopWatchHours = stopWatch.Elapsed.Hours.ToString();
            StopWatchMinutes = stopWatch.Elapsed.Minutes.ToString();
            StopWatchSeconds = stopWatch.Elapsed.Seconds.ToString();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                StopWatchHours = stopWatch.Elapsed.Hours.ToString();
                StopWatchMinutes = stopWatch.Elapsed.Minutes.ToString();
                StopWatchSeconds = stopWatch.Elapsed.Seconds.ToString();
                return true;
            });
        }


        private void StopTime()
        {
            started = false;
            TimerText = "START";
            OnPropertyChanged("TextButton");
            stop_timed = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
            Response = stopWatch.Elapsed.Hours.ToString();
            stopWatch.Stop();
            stopWatch.Reset();
            StopWatchSeconds = "0";
            StopWatchMinutes = "0";
            StopWatchHours = "0";
            IsAttached = true;
        }
        private async void GetResponseRequest()
        {
            try
            {
                Projects = await Requetes.GetProjects();
            }

            catch (Exception)
            {
                await App.Current.MainPage.DisplayAlert("Oh oh ! Une erreur est survenue !", "Cette erreur peut �tre d�e � un probl�me de connexion � Internet, ou � une erreur interne.", "Retour");
                Logout();
            }
        }

        private async void ListTasks(Project p)
        {
            try
            {
                Tasks = await Requetes.GetTasks(p.id);
            }
            catch (Exception)
            {
                await App.Current.MainPage.DisplayAlert("Oh oh ! Une erreur est survenue !", "Cette erreur peut �tre d�e � un probl�me de connexion � Internet, ou � une erreur interne.", "Retour");
                Logout();
            }
        }

        private async void AddProject()
        {
            await App.Current.MainPage.Navigation.PushAsync(new NewProject());
        }

        // Supprime toutes les pages de la Navigation et renvoie vers Connexion, emp�chant un retour arri�re
        private async void Logout() 
        {
            await App.Current.MainPage.Navigation.PopToRootAsync();
        }


        private async void DetailsTache(Task task)
        {
            await App.Current.MainPage.Navigation.PushAsync(new DetailsTask(task));
        }


        private async void AskUser(Project project)
        {
            bool reponse = await App.Current.MainPage.DisplayAlert(project.name, "Acc�der � la page du projet ?", "Oui", "Non");

            if (reponse)
            {
                await App.Current.MainPage.Navigation.PushAsync(new DetailsProject(project));
            }
        }

        private async void EditProfil()
        {
            await App.Current.MainPage.Navigation.PushAsync(new UserProfile());
        }
        private async void GoToGraphes()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Graphes());
        }
    }
}