	.file	"compressed_assemblies.x86.x86.s"
	.include	"compressed_assemblies.x86-data.inc"

	.section	.data.compressed_assembly_descriptors,"aw",@progbits
	.type	.L.compressed_assembly_descriptors, @object
	.p2align	2
.L.compressed_assembly_descriptors:
	/* 0: FormsViewGroup.dll */
	/* uncompressed_file_size */
	.long	16384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_0

	/* 1: Java.Interop.dll */
	/* uncompressed_file_size */
	.long	163840
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_1

	/* 2: Microcharts.Droid.dll */
	/* uncompressed_file_size */
	.long	5632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_2

	/* 3: Microcharts.Forms.dll */
	/* uncompressed_file_size */
	.long	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_3

	/* 4: Microcharts.dll */
	/* uncompressed_file_size */
	.long	61952
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_4

	/* 5: Mono.Android.dll */
	/* uncompressed_file_size */
	.long	2190848
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_5

	/* 6: Mono.Security.dll */
	/* uncompressed_file_size */
	.long	121856
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_6

	/* 7: SkiaSharp.Views.Android.dll */
	/* uncompressed_file_size */
	.long	50560
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_7

	/* 8: SkiaSharp.Views.Forms.dll */
	/* uncompressed_file_size */
	.long	156560
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_8

	/* 9: SkiaSharp.dll */
	/* uncompressed_file_size */
	.long	414608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_9

	/* 10: Storm.Mvvm.Forms.dll */
	/* uncompressed_file_size */
	.long	14336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_10

	/* 11: System.Core.dll */
	/* uncompressed_file_size */
	.long	343552
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_11

	/* 12: System.Drawing.Common.dll */
	/* uncompressed_file_size */
	.long	24576
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_12

	/* 13: System.Net.Http.dll */
	/* uncompressed_file_size */
	.long	218112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_13

	/* 14: System.Numerics.dll */
	/* uncompressed_file_size */
	.long	25600
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_14

	/* 15: System.Runtime.Serialization.dll */
	/* uncompressed_file_size */
	.long	400384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_15

	/* 16: System.ServiceModel.Internals.dll */
	/* uncompressed_file_size */
	.long	55808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_16

	/* 17: System.Xml.dll */
	/* uncompressed_file_size */
	.long	888832
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_17

	/* 18: System.dll */
	/* uncompressed_file_size */
	.long	759296
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_18

	/* 19: TimeTracker.Apps.Droid.dll */
	/* uncompressed_file_size */
	.long	485376
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_19

	/* 20: TimeTracker.Apps.dll */
	/* uncompressed_file_size */
	.long	7168
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_20

	/* 21: Xamarin.AndroidX.Activity.dll */
	/* uncompressed_file_size */
	.long	53248
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_21

	/* 22: Xamarin.AndroidX.AppCompat.AppCompatResources.dll */
	/* uncompressed_file_size */
	.long	17408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_22

	/* 23: Xamarin.AndroidX.AppCompat.dll */
	/* uncompressed_file_size */
	.long	463360
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_23

	/* 24: Xamarin.AndroidX.CardView.dll */
	/* uncompressed_file_size */
	.long	17920
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_24

	/* 25: Xamarin.AndroidX.CoordinatorLayout.dll */
	/* uncompressed_file_size */
	.long	79360
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_25

	/* 26: Xamarin.AndroidX.Core.dll */
	/* uncompressed_file_size */
	.long	585728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_26

	/* 27: Xamarin.AndroidX.CustomView.dll */
	/* uncompressed_file_size */
	.long	9216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_27

	/* 28: Xamarin.AndroidX.DrawerLayout.dll */
	/* uncompressed_file_size */
	.long	44032
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_28

	/* 29: Xamarin.AndroidX.Fragment.dll */
	/* uncompressed_file_size */
	.long	175104
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_29

	/* 30: Xamarin.AndroidX.Legacy.Support.Core.UI.dll */
	/* uncompressed_file_size */
	.long	15872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_30

	/* 31: Xamarin.AndroidX.Lifecycle.Common.dll */
	/* uncompressed_file_size */
	.long	15360
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_31

	/* 32: Xamarin.AndroidX.Lifecycle.LiveData.Core.dll */
	/* uncompressed_file_size */
	.long	16384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_32

	/* 33: Xamarin.AndroidX.Lifecycle.ViewModel.dll */
	/* uncompressed_file_size */
	.long	17408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_33

	/* 34: Xamarin.AndroidX.Loader.dll */
	/* uncompressed_file_size */
	.long	36864
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_34

	/* 35: Xamarin.AndroidX.RecyclerView.dll */
	/* uncompressed_file_size */
	.long	424448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_35

	/* 36: Xamarin.AndroidX.SavedState.dll */
	/* uncompressed_file_size */
	.long	13312
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_36

	/* 37: Xamarin.AndroidX.SwipeRefreshLayout.dll */
	/* uncompressed_file_size */
	.long	40448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_37

	/* 38: Xamarin.AndroidX.ViewPager.dll */
	/* uncompressed_file_size */
	.long	57856
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_38

	/* 39: Xamarin.Essentials.dll */
	/* uncompressed_file_size */
	.long	25600
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_39

	/* 40: Xamarin.Forms.Core.dll */
	/* uncompressed_file_size */
	.long	1209344
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_40

	/* 41: Xamarin.Forms.Platform.Android.dll */
	/* uncompressed_file_size */
	.long	936960
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_41

	/* 42: Xamarin.Forms.Platform.dll */
	/* uncompressed_file_size */
	.long	264112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_42

	/* 43: Xamarin.Forms.Xaml.dll */
	/* uncompressed_file_size */
	.long	103424
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_43

	/* 44: Xamarin.Google.Android.Material.dll */
	/* uncompressed_file_size */
	.long	258048
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_44

	/* 45: Xamarin.Google.Guava.ListenableFuture.dll */
	/* uncompressed_file_size */
	.long	18072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_45

	/* 46: mscorlib.dll */
	/* uncompressed_file_size */
	.long	2058240
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_46

	.size	.L.compressed_assembly_descriptors, 564
	.section	.data.compressed_assemblies,"aw",@progbits
	.type	compressed_assemblies, @object
	.p2align	2
	.global	compressed_assemblies
compressed_assemblies:
	/* count */
	.long	47
	/* descriptors */
	.long	.L.compressed_assembly_descriptors
	.size	compressed_assemblies, 8
